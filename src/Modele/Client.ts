import { Address } from './Address';
import { Roles } from './Roles';
export class Client{
    id: number;
    username: string;
    password: string;
    roles: Array<Roles>;
    address: Address;
   /* name: string;
    email: string;
    phoneNumber: string;*/

}

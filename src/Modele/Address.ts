export class Address{
    id: number;
    streetNumber: string;
    street: string;
    postalCode: number;
    city: string;

}

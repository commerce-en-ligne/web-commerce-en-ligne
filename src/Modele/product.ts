export class Product{
    id: number;
    designation: string;
    description: string;
    prix: number;
    quantity: number;
    photo: string;
    promotion: boolean;
    prixPromo: number;
    quantityOrder: number;


}

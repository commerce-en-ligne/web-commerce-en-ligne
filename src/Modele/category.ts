
import { Product } from './product';
export class Category {
    id: number;
    name: string;
    description: string;
    productsList: Array<Product>;
}

import { Product } from './product';

export class ItemProduct{
    quantity: number;
    prix: number;
    prixPromo: number;
    product: Product;

}

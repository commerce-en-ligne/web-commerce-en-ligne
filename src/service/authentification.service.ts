import { Client } from './../Modele/Client';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  public host = 'http://localhost:8080';

  private users = [
    {username: 'admin', password: '1234', roles: ['ADMIN', 'USER']},
    {username: 'user1', password: '1234', roles: ['USER']},
    {username: 'user2', password: '1234', roles: ['USER']},
  ];
  public isAthenticated: boolean;
  public userAuthenticated;
  public token: string;
  constructor(private http: HttpClient) {  }
/*
    public login(username: string, password: string) {
      let user;
      this.users.forEach(u => {
        if (u.username == username && u.password == password) {
          user = u;
          this.token = btoa(JSON.stringify({username: u.username, roles: u.roles}));
        }
      });
      if (user) {
        this.isAthenticated = true;
        this.userAuthenticated = user;
      } else{
        this.isAthenticated = false;
        this.userAuthenticated = undefined;
      }

    }*/

    public login(user: Client) {
      if (user) {
        this.isAthenticated = true;
        this.userAuthenticated = user;
        this.token = btoa(JSON.stringify({id: user.id, username: user.username, roles: user.roles}));
      } else{
        this.isAthenticated = false;
        this.userAuthenticated = undefined;
      }

    }

    public authentification(url, dataForm){
      return this.http.post<Client>(this.host + url, dataForm);
    }

    public isAdmin(){
      if (this.userAuthenticated) {
        if (this.userAuthenticated.roles.indexOf('ADMIN') > -1) {
          return true;
        }
        return false;
      }
    }

    public saveAuthenticatedUser() {
      if (this.userAuthenticated) {
        localStorage.setItem('authToken', this.token);
      }
    }

    public loadAuthenticatedUserFromLocaStorage(){
      const tokenStorage  = localStorage.getItem('authToken');
      if (tokenStorage) {
        const user = JSON.parse(atob(tokenStorage));
        this.userAuthenticated = {id: user.id, username: user.username, roles: user.roles};
        this.isAthenticated = true;
        this.token = tokenStorage;
      }
    }

    public removeTokenFromLocalStorage(){
      localStorage.removeItem('authToken');
      this.isAthenticated = false;
      this.token = undefined;
      this.userAuthenticated = undefined;
    }

    getOneUser(url){
      return this.http.get<Client>(this.host + url);
    }

    updateClient(url, client){
      return this.http.put<boolean>(this.host + url, client);
    }

}

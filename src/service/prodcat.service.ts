import { Injectable } from '@angular/core';
import { Product } from 'src/Modele/product';
import { Category } from 'src/Modele/category';
import { HttpClient, HttpEvent, HttpRequest} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProdCatService {
  public host = 'http://localhost:8080';

  constructor( private http: HttpClient) { }

  public getProducts(url){
    return this.http.get<Product>(this.host + url);
  }

  public getCategories(url){
    return this.http.get<Category>(this.host + url);
  }

  public uploadPhotoProduct(currentFileUpload: File, idProduct): Observable<HttpEvent<{}>> {
    const formdata: FormData = new FormData();
    formdata.append('currentFileUpload', currentFileUpload);

    const req = new HttpRequest('POST', this.host + '/uploadPhoto/' + idProduct, formdata, {
      reportProgress: true,
      responseType: 'text'
    });
    return this.http.request(req);
  }

  public updateProduct(url, data) {

    return this.http.put(this.host + url, data);
  }


}

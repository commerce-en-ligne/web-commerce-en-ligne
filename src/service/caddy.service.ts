import { ItemProduct } from './../Modele/ItemProduct';
import { Caddy } from './../Modele/Caddy';
import { Injectable } from '@angular/core';
import { Product } from 'src/Modele/product';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CaddyService {
  currentCaddyName = 'Caddy1';
  caddy: Caddy = new Caddy();
  // cart: number;
  public host = 'http://localhost:8080';
  // nbProductInCart: number;

  constructor(private http: HttpClient) {
    const caddies = localStorage.getItem('myCaddy');
    if (caddies){
      this.caddy = JSON.parse(caddies);
    } else {
      // const caddy = new Caddy(this.currentCaddyName);
      // this.caddy = caddy;
    }
  }

  public addProductToCaddy(product: Product){
    const caddy = this.caddy.listProductInCaddy.filter(p => p.id == product.id);
    if (caddy.length != 0){
      caddy.forEach(c => c.quantity += product.quantityOrder);
    } else{
      this.caddy.listProductInCaddy.push(product);
      this.caddy.totalPrix = this.getTotalCurrentCaddy();
      this.saveCaddies();
    }
  }

  public saveCaddies(){
    localStorage.setItem('myCaddy', JSON.stringify(this.caddy));
  }

  getCurrentCaddy(){
    console.log(this.caddy);
    return this.caddy;
  }

  public getTotalCurrentCaddy(){
    let total = 0;
    this.getCurrentCaddy().listProductInCaddy.forEach(t => {
      if (t.promotion == true) {
        total += t.prixPromo * t.quantityOrder;
      }else{
        total += t.prix * t.quantityOrder;
      }

    });
    return total;
  }

  removeProductIncaddy(product: Product){
    const caddy = this.caddy.listProductInCaddy.filter(p => p.id == product.id).lastIndexOf;
    if (caddy.length != 0){
      for (let index = 0; index < this.caddy.listProductInCaddy.length; index++) {
        if (this.caddy.listProductInCaddy[index] == product) {
          this.caddy.listProductInCaddy.splice(index, 1);
          index--;
        }
      }
      this.caddy.totalPrix = this.getTotalCurrentCaddy();
      this.saveCaddies();
    }

  }

  removeAllProductInCart(){
    localStorage.removeItem('myCaddy');
  }

  addPaymentCC(url, data) {
    return this.http.post<boolean>(this.host + url, data);
  }

  addPaymentPp(url, data){
    return this.http.post<boolean>(this.host + url, data);
  }
  getOrdersUser(url){
    return this.http.get<any>(this.host + url);
  }

}

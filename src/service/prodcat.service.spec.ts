import { TestBed } from '@angular/core/testing';

import { ProdCatService } from './prodcat.service';

describe('ProdCatService', () => {
  let service: ProdCatService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProdCatService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

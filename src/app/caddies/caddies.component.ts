import { AuthentificationService } from 'src/service/authentification.service';
import { CaddyService } from './../../service/caddy.service';
import { Caddy } from './../../Modele/Caddy';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ProdCatService } from 'src/service/prodcat.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-caddies',
  templateUrl: './caddies.component.html',
  styleUrls: ['./caddies.component.css']
})
export class CaddiesComponent implements OnInit {
  timeStamp: any;
  mode: number;
  formPayment: FormGroup;
  formPpPayment: FormGroup;
  cardNumber = '^[0-9]{9}$';
  code = '^[0-9]{3}$';

  constructor(
    public caddyService: CaddyService,
    public prodcatService: ProdCatService,
    public router: Router,
    public route: ActivatedRoute,
    private formBuilder: FormBuilder,
    public authService: AuthentificationService
    ) { }

  ngOnInit(): void {
    this.mode = 0;
    this.formPayment = this.formBuilder.group({
      cardNumber: ['', Validators.compose([Validators.required, Validators.pattern(this.cardNumber),
         Validators.minLength(9), Validators.maxLength(9)])],
      code: ['', Validators.compose([Validators.required, Validators.pattern(this.code),
         Validators.minLength(3), Validators.maxLength(3)])],
      dateExpiration: ['', [Validators.required]],
      typePay: 'CC',
      datePayment: new Date().toISOString().split('T', 1)[0],
      prixTotal: this.caddyService.getTotalCurrentCaddy(),
      listProductInCaddy: this.formBuilder.array(this.caddyService.getCurrentCaddy().listProductInCaddy),
      idUser: this.authService.userAuthenticated.id
    });

    this.formPpPayment = this.formBuilder.group({
      accountNumber: ['', Validators.compose([Validators.required, Validators.pattern(this.cardNumber),
         Validators.minLength(9), Validators.maxLength(9)])],
      typePay: 'PP',
      datePayment: new Date().toISOString().split('T', 1)[0],
      prixTotal: this.caddyService.getTotalCurrentCaddy(),
      listProductInCaddy: this.formBuilder.array(this.caddyService.getCurrentCaddy().listProductInCaddy),
      idUser: this.authService.userAuthenticated.id
    });
  }

  getTS() {
    return this.timeStamp;
  }
  removeProductIncaddy(product){
    this.caddyService.removeProductIncaddy(product);
  }

  login(){
    this.router.navigateByUrl('login');
  }

  continuePurchase(){
    this.router.navigateByUrl('products/1/0');
  }

  toOrder(caddy){
    if (this.authService.isAthenticated == true){
      this.mode = 1;
    } else{
      this.router.navigateByUrl('/login');
    }
  }

  onPayment(formPayment){
    if (formPayment.typePay == 'CC') {
      if (formPayment.dateExpiration.valueOf() >= formPayment.datePayment.valueOf()) {
        return this.caddyService.addPaymentCC('/addPaymentCc', formPayment)
          .subscribe(data => {
            this.router.navigateByUrl('/orders/2');
            this.caddyService.removeAllProductInCart();
          }, err => {
          console.log(err);
        });
      }
    }
    return this.caddyService.addPaymentPp('/addPaymentPp', formPayment)
    .subscribe(data => {
      this.router.navigateByUrl('/orders/2');
      this.caddyService.removeAllProductInCart();
    }, err => {
      console.log(err);
    });
  }

  creditCart(){
    this.mode = 2;
  }

  payPal(){
    this.mode = 3;
  }
}

import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ProdCatService } from 'src/service/prodcat.service';
import { Product } from 'src/Modele/product';
import { AuthentificationService } from 'src/service/authentification.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  public currentProduct: Product;
  mode: number;
  selectedFile: any;
  editPhoto: boolean;
  progress: any;
  currentFileUpload: any;
  photo: any;
  title: string;
  timeStamp = 0;


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public prodcatService: ProdCatService,
    public authService: AuthentificationService
  ) { }

  ngOnInit(): void {
    const idProduct = this.route.snapshot.params.id;
    this.prodcatService.getProducts('/details-product/' + idProduct).subscribe(data => {
      this.currentProduct = data;
      this.mode = 0;
    }, err => {
      console.log(err);
    });
  }
  onEditPhoto(p){
    this.currentProduct = p;
    this.editPhoto = true;
  }

  onSelectedFile(event){
    this.selectedFile = event.target.files;
  }

  uploadPhoto(){
    this.progress = 0;
    this.currentFileUpload = this.selectedFile.item(0);
    this.prodcatService.uploadPhotoProduct(this.currentFileUpload, this.currentProduct.id).subscribe (event => {
      if (event.type === HttpEventType.UploadProgress){
        this.progress = Math.round(100 * event.loaded / event.total);
        console.log(this.progress);
      } else if (event instanceof HttpResponse) {
        this.timeStamp = Date.now();
      }
    }, err => {
      alert('problème telechargement');
    });

  }

  getTS() {
    return this.timeStamp;
  }

  editProduct() {
    this.mode = 1;
  }

  public addProductOnShoppingCart(p) {
      console.log(p);
  }

  updateProduct(productUpdate: Product) {
    return this.prodcatService.updateProduct('/updateProduct/' + this.currentProduct.id, productUpdate)
    .subscribe(data => {
      if (data == true) {
        this.mode = 0;
      }
    }, err => {
      console.log(err);
    });
  }


}

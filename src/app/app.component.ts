import { CaddyService } from './../service/caddy.service';
import { Component, OnInit } from '@angular/core';
import { Category } from 'src/Modele/category';
import { ProdCatService } from 'src/service/prodcat.service';
import { ActivatedRoute, Router} from '@angular/router';
import { AuthentificationService } from 'src/service/authentification.service';
import { Caddy } from 'src/Modele/Caddy';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  categories: Category;
  currentCategory: Category;
  paramIdProduct: any;
  cart: any;
  timeStamp = 0;

  constructor(
    private prodcatService: ProdCatService,
    private router: Router,
    public authService: AuthentificationService,
    public caddyService: CaddyService,
    private route: ActivatedRoute
    ){}

  ngOnInit(): void {
    const url = window.location.href;
    const lien = url.split('/')[3];
    if (lien == 'products') {
      this.paramIdProduct = url.substring(url.length - 1);
    }
    this.authService.loadAuthenticatedUserFromLocaStorage();
    this.getCategories();
  }
  getCategories() {
    this.prodcatService.getCategories('/category')
    .subscribe(data => {
      this.categories = data;
    }, err => {
      console.log('Erreur');
    });
  }

  getProductByCategory(c){
    this.currentCategory = c;
    this.paramIdProduct = undefined;
    this.router.navigateByUrl('/products/2/' + c.id);

  }
  getProducts(){
    this.currentCategory = undefined;
    this.paramIdProduct = undefined;
    this.router.navigateByUrl('/products/1/0');
  }

  getProductsPromo(){
    this.currentCategory = undefined;
    this.router.navigateByUrl('/products/3/0');
  }

  onLogout(){
    this.authService.removeTokenFromLocalStorage();
    this.router.navigateByUrl('/login');
  }

  onLogin(){
    this.router.navigateByUrl('/login');
  }

  onOrders(){
    this.router.navigateByUrl('/orders/2');
  }

  onInfos(){
    this.router.navigateByUrl('/orders/1');
  }

  getTS() {
    return this.timeStamp;
  }
}

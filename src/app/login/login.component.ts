import { AuthentificationService } from './../../service/authentification.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private authService: AuthentificationService,
    private router: Router
    ) { }

  ngOnInit(): void {
  }

  onLogin(dataForm: any){
    return this.authService.authentification('/login', dataForm)
      .subscribe(data => {
        this.authService.login(data);
        if (this.authService.isAthenticated) {
          this.authService.saveAuthenticatedUser();
          this.router.navigateByUrl('');
        }
    }, err => {
      console.log(err);
    });
  }

}

import { Router, NavigationEnd, ActivatedRoute, NavigationStart } from '@angular/router';
import { Client } from './../../Modele/Client';
import { AuthentificationService } from 'src/service/authentification.service';
import { Component, OnInit } from '@angular/core';
import { CaddyService } from 'src/service/caddy.service';
import { ProdCatService } from 'src/service/prodcat.service';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  listProducts: any;
  mode: number;
  idUser: number;
  client: Client;
  FormEditClient: FormGroup;
  paramOrder: any;


  constructor(
    private authService: AuthentificationService,
    private caddyService: CaddyService,
    public prodcatService: ProdCatService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute

  ) { }


  ngOnInit(): void {
    this.authService.loadAuthenticatedUserFromLocaStorage();
    this.idUser = this.authService.userAuthenticated.id;
    this.getOrdersUser();
    this.getOneUser();

    this.FormEditClient = this.formBuilder.group({
      username: [''],
      password: [''],
      roles: [''],
      address: this.formBuilder.group({
        streetNumber: [''],
        street: [''],
        postalCode: [''],
        city: ['']
      })
    });
    this.paramOrder = this.route.snapshot.url[1].path;
    if (this.paramOrder == '1') {
      this.infoUser();
    } else if (this.paramOrder == '2'){
      this.orders();
    }
  }



  public getOrdersUser(){
    this.idUser = this.authService.userAuthenticated.id;
    return this.caddyService.getOrdersUser('/getOrderByUser/' + this.idUser)
      .subscribe(data => {
       /* for (let index = 0; index < data.length; index++) {
         this.listProducts = data[index].listProductInCaddy;
        }*/
        for (const iterator of data) {
          this.listProducts = iterator.listProductInCaddy;
        }
      }, err => {
        console.log(err);
      });

  }
getOneUser(){
  const idUser = this.authService.userAuthenticated.id;
  return this.authService.getOneUser('/users/' + this.idUser)
      .subscribe(data => {
        this.client = data;
      }, err => {
        console.log(err);
      });
}

updateClient(idClient){
  this.mode = 3;
}
onUpdateClient(){
console.log(this.client);
return this.authService.updateClient('/updateUser/' + this.idUser, this.client)
  .subscribe(data => {
    this.getOneUser();
    this.mode = 1;
  }, err => {
    console.log(err);
  });

}

  orders() {
    this.mode = 2;
    this.router.navigateByUrl('/orders/2');
  }

  infoUser() {
    this.mode = 1;
    this.router.navigateByUrl('/orders/1');
  }

}

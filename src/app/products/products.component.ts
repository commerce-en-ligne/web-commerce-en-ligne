import { CaddyService } from './../../service/caddy.service';
import { AuthentificationService } from './../../service/authentification.service';
import { Component, OnInit } from '@angular/core';
import { Product } from 'src/Modele/product';
import { ProdCatService } from 'src/service/prodcat.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products: Product;
  editPhoto: boolean;
  currentProduct: Product;
  selectedFile: any;
  progress: any;
  currentFileUpload: any;
  photo: any;
  title: string;
  timeStamp = 0;


  constructor(
    public prodcatService: ProdCatService,
    private route: ActivatedRoute,
    private router: Router,
    public authService: AuthentificationService,
    public caddyService: CaddyService
    ) {

    }

  ngOnInit(): void {
      this.router.events.subscribe((val) => {
        if (val instanceof NavigationEnd) {
              const url = val.url;
              const p1 = this.route.snapshot.params.p1;
              if (p1 == 1) {
                this.title = 'Produits Disponibles';
                this.getProducts('/products');
              } else if (p1 == 2){
                const idCategory = this.route.snapshot.params.p2;
                this.title = 'Produits de la categorie ' + idCategory;
                this.getProducts('/productByCategory/' + idCategory);
              } else if (p1 == 3){
                this.title = 'Produits en promotion';
                this.getProducts('/productsPromotion');
              }  else if (p1 == 4){
                this.title = 'Produits recherchés';
                this.getProducts('/searchProduct/{description}');
              }
        }
      });
      const pr1 = this.route.snapshot.params.p1;
      if (pr1 == 1) {
        this.title = 'Produits Disponibles';
        this.getProducts('/products');
      }else if (pr1 == 2){
        const idCategory = this.route.snapshot.params.p2;
        this.title = 'Produits de la categorie ' + idCategory;
        this.getProducts('/productByCategory/' + idCategory);
      }else if (pr1 == 3){
        this.title = 'Produits en promotion';
        this.getProducts('/productsPromotion');
      }  else if (pr1 == 4){
        this.title = 'Produits recherchés';
        this.getProducts('/searchProduct/{description}');
      }
  }
  getProducts(url) {
    this.prodcatService.getProducts(url)
    .subscribe(data => {
      this.products = data;
    }, err => {
      console.log('erreur');
    });
  }

  onEditPhoto(p){
    this.currentProduct = p;
    this.editPhoto = true;
  }

  onSelectedFile(event){
    this.selectedFile = event.target.files;
  }

  uploadPhoto(){
    this.progress = 0;
    this.currentFileUpload = this.selectedFile.item(0);
    this.prodcatService.uploadPhotoProduct(this.currentFileUpload, this.currentProduct.id).subscribe (event => {
      if (event.type === HttpEventType.UploadProgress){
        this.progress = Math.round(100 * event.loaded / event.total);
        console.log(this.progress);
      } else if (event instanceof HttpResponse) {
        this.timeStamp = Date.now();
      }
    }, err => {
      alert('problème telechargement');
    });

  }

  getTS() {
    return this.timeStamp;
  }


  public addProductOnShoppingCart(product: Product) {
     this.caddyService.addProductToCaddy(product);
  }
   public onDetailProduct(p: Product) {
    this.router.navigateByUrl('product-detail/' + p.id);
  }
}
